http-requests:

	1) Get all users (feature for the admin:

		curl http://localhost:8080/api/user
	
	2) POST (create new user):

		 $curl --location --request POST 'http://localhost:8080/api/user' --header 'Content-Type: application/json' --data-raw '{"name":"Peter", "email":"peter@gmail.com"}'
