package com.martdevem.springbootsimpleapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSimpleApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSimpleApiRestApplication.class, args);
	}

}
