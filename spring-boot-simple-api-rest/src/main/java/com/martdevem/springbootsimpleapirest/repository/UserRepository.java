package com.martdevem.springbootsimpleapirest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.martdevem.springbootsimpleapirest.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
}
